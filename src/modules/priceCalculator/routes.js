import PriceCalculator from "../priceCalculator/views/PriceCalculator.vue";

const routes = [
  {
    path: "/",
    name: "PriceCalculator",
    component: PriceCalculator,
  },
];

export default routes;
