import QuoteHistory from "../quoteHistory/views/QuoteHistory.vue";

const routes = [
  {
    path: "/history",
    name: "QuoteHistory",
    component: QuoteHistory,
  },
];

export default routes;
