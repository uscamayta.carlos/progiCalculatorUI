# Car Auction Price Calculator

## Description

This application allows a buyer to calculate the total price of a vehicle at a car auction. It uses Vue.js for building the user interface, Vuetify as the design framework, and Vuex for state management.

## Technologies Used

- **Vue.js**: A progressive JavaScript framework used for building user interfaces.
- **Vuetify**: A Material Design component framework for Vue.js.
- **Vuex**: A state management library for Vue.js applications.
- **JavaScript**: The programming language used for developing the application.
- **HTML5**: The markup language used for structuring the content of the web.
- **CSS3**: The style sheet language used for designing and presenting the web.

## Application Objective

The primary goal of this application is to provide buyers with an easy-to-use tool for calculating the total price of a vehicle at a car auction, taking into account various factors such as the base price, auction fees, taxes, and other potential additional costs.

## Features

- **User-Friendly Interface**: Thanks to Vuetify, the application features an intuitive and attractive user interface.
- **Centralized State Management**: Utilizes Vuex to manage the application's state, ensuring that data is consistent and reactive throughout the application.
- **Total Price Calculation**: Allows users to input different parameters and automatically calculates the total price of the vehicle.
- **Responsive Design**: The application is optimized to work on different devices, including mobile phones and tablets.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/uscamayta.carlos/progiCalculatorUI.git

2. Install the dependencies:
   ```bash
   npm install
   
1. Start the application:
   ```bash
   npm start
